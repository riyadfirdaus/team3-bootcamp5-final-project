import React from "react";
import { Route, Routes } from "react-router-dom";
import PageNotFound from "../pages/404";
import AdminHome from "../pages/Admin/AdminHome";
import CheckOut from "../pages/CheckOut";
import ConfimationSuccessPage from "../pages/ConfimationSuccessPage";
import CourseDetail from "../pages/Course/CourseDetail";
import CourseMenu from "../pages/Course/CourseMenu";
import Invoice from "../pages/Invoice";
import InvoiceDetails from "../pages/Invoice/[detail]";
import LandingPage from "../pages/LandingPage";
import LoginRegister from "../pages/LoginRegister";
import MyClass from "../pages/MyClass/MyClass";
import ResetPasswordEmail from "../pages/ResetPasswordEmail";

function routes() {
  return (
    <Routes>
      {/* Admin View */}
      <Route path="/admin/*" element={<AdminHome />} />

      {/* User View */}
      <Route path="/" element={<LandingPage />} />

      {/* Modul Kelas */}
      <Route path="/coursemenu/:id" element={<CourseMenu />} />
      <Route path="/coursedetail/:id" element={<CourseDetail />} />
      <Route path="/myclass" element={<MyClass />} />

      {/* Modul Checkout */}
      <Route path="/checkout" element={<CheckOut />} />
      <Route path="/invoice" element={<Invoice />} />
      <Route path="/invoice/:id" element={<InvoiceDetails />} />

      {/* Modul Login & Register */}
      <Route path="/login" element={<LoginRegister page={"login"} />} />
      <Route path="/register" element={<LoginRegister page={"register"} />} />
      <Route
        path="/resetpassemail"
        element={<ResetPasswordEmail page={"email"} />}
      />
      <Route
        path="/resetpass"
        element={<ResetPasswordEmail page={"password"} />}
      />
      <Route
        path="/successemail"
        element={<ConfimationSuccessPage page={"email"} />}
      />
      <Route
        path="/successpurchase"
        element={<ConfimationSuccessPage page={"purchase"} />}
      />
      <Route
        path="/myclass"
        element={<MyClass />}
      />
      {/* handler page not found */}
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
}

export default routes;
