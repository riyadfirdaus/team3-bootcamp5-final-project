import React from "react";
import { Route, Routes } from "react-router-dom";
import PageNotFound from "../pages/404";
import InvoicePage from "../pages/Admin/InvoicePage";
import UserPage from "../pages/Admin/UserPage";
import PaymentMethod from "../pages/Admin/PaymentMethod";
import DashboardPage from "../pages/Admin/DashboardPage";

function AdminRoutes() {
  return (
    <Routes>
      {/* Admin View */}
      <Route path="/" element={<UserPage />} />
      <Route path="/invoicepage" element={<InvoicePage />} />
      <Route path="/paymentmethod" element={<PaymentMethod />} />
      <Route path="/userpage" element={<UserPage />} />

      {/* handler page not found */}
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
}

export default AdminRoutes;
