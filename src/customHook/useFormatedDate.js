import { useState, useEffect } from 'react';

const useFormattedDate = (dateString) => {
  const [formattedDate, setFormattedDate] = useState('');

  useEffect(() => {
    const date = new Date(dateString);

    const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const monthsOfYear = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    const dayOfWeek = daysOfWeek[date.getUTCDay()];
    const dayOfMonth = date.getUTCDate();
    const monthOfYear = monthsOfYear[date.getUTCMonth()];
    const year = date.getUTCFullYear();

    const formattedDate = `${dayOfWeek}, ${dayOfMonth} ${monthOfYear} ${year}`;
    setFormattedDate(formattedDate);
  }, [dateString]);

  return formattedDate;
};

export default useFormattedDate;
