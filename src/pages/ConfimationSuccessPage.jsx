import { Typography, Box, Container, TextField, Button } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import LogoOnlyNavbar from "../components/LogoOnlyNavbar";
import jempol from '../assets/jempol.png'
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

const styles = {
    container: {
        width: '70%',
        marginTop: '10%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    typoemailAtas: {
        mt: 3,
        mb: 1,
        fontSize: { sm: '24px', xs: '12px' },
        alignSelf: 'center'
    },
    typoemailBawah: {
        mb: 3,
        fontSize: { sm: '16px', xs: '8px' },
        textAlign: 'center'
    },
    buttonBox: {
        display: 'flex',
        justifyContent: 'center',
        gap: '20px',
        mt: 3
    },
    image: {
        maxWidth: '225px',
        alignSelf: 'center'
    }
}
const ConfimationSuccessPage = ({ page }) => {
    console.log(page)
    const navigate = useNavigate()
    return (
        <>
            <LogoOnlyNavbar />
            {page === 'email' ?
                <Container sx={styles.container} >
                    <Box sx={{
                        display: 'flex',
                        width: "100%",
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignitems: 'center'
                        
                    }}>
                    <img src={jempol} style={styles.image} />
                    <Typography sx={styles.typoemailAtas} color='primary'>Email Confirmation Success</Typography>
                    <Typography sx={styles.typoemailAtas} style={{textAlign: 'center'}}>
                        Your email already! Please login first to access the web
                    </Typography>
                    </Box>
                    <Box>
                        <Box fullwidth sx={styles.buttonBox}>
                            <Button variant="contained" color="primary" onClick={() => navigate('/login')}>Login Here</Button>
                        </Box>
                    </Box>
                </Container>
                :
                <Container sx={styles.container} >
                    <Box sx={{
                        display: 'flex',
                        width: "100%",
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignitems: 'center'
                        
                    }}>
                    <img src={jempol} style={styles.image} />
                    <Typography sx={styles.typoemailAtas} color='primary'>Purchase Successfully</Typography>
                    <Typography sx={styles.typoemailAtas} style={{textAlign: 'center'}}>
                    That’s Great! We’re ready for driving day
                    </Typography>
                    </Box>
                    <Box>
                        <Box fullwidth sx={styles.buttonBox}>
                        <Button sx={{padding: '15px'}} variant="outlined" color="primary" onClick={() => navigate('/')}>
                            <HomeIcon sx={{marginRight: '5px'}}/>
                            Back to Home
                        </Button>
                        <Button variant="contained" color="primary" onClick={() => navigate('/invoice')}>
                            <ArrowForwardIcon sx={{marginRight: '5px'}}/>
                            Open Invoice</Button>
                        </Box>
                    </Box>
                </Container>
            }
        </>
    );
};

export default ConfimationSuccessPage;
