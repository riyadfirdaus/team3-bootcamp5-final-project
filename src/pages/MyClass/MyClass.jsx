import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Layout from "../../components/Layout";
import ClassCard from "../../components/MyClass/ClassCard";

function MyClass() {
  const base = process.env.REACT_APP_BASE_API_URL;
  const { currentUser } = useSelector((state) => state.auth);
  const [classes, setClasses] = useState([]);

  const fetchClass = async () => {
    try {
      const resp = await axios.get(
        `${base}/api/User/GetUserClass?Id=${currentUser.id}`
      );
      setClasses(resp.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchClass();
  }, []);
  return (
    <Layout>
      {classes?.map((value, index) => {
        return <ClassCard key={index} data={value} />;
      })}
    </Layout>
  );
}

export default MyClass;
