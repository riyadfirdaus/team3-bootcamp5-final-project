import React, { useEffect, useState } from "react";
import AnotherCourseList from "../components/CourseList/AnotherCourseList";
import Benefit from "../components/Benefit";
import Layout from "../components/Layout";
import CarListType from "../components/CarTypeList/CarTypeList";
import Hero from "../components/Hero";
import { useDispatch, useSelector } from "react-redux";
import { fetchCourses, selectAllCourses } from "../Redux/CourseSlice";

function LandingPage() {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state) => state.auth);
  console.log(currentUser);

  const [courseData, setCourseData] = useState();
  const data = useSelector(selectAllCourses);

  const FetchData = async () => {
    try {
      setCourseData(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    dispatch(fetchCourses());
    FetchData();
  }, [dispatch]);

  return (
    <Layout title="Home">
      <Hero />
      <AnotherCourseList
        title="Join Us for the Course"
        courseData={courseData}
      />
      <Benefit />
      <CarListType title="More car type you can choose" />
    </Layout>
  );
}

export default LandingPage;
