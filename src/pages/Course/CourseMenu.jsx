import { Box, Divider } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { selectTypesById } from "../../Redux/CarTypeSlice";
import AnotherCourseList from "../../components/CourseList/AnotherCourseList";
import DescriptionBox from "../../components/DescriptionBox";
import Layout from "../../components/Layout";

function CourseMenu() {
  const props = useLocation().state;

  //Data Selector
  const data = useSelector((state) => selectTypesById(state, props.id));
  console.log(data);

  // Fetch Course List
  const [courseData, setCourseData] = useState();

  const FetchData = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_BASE_API_URL}/api/User/GetCourseByType?id=${props.id}`
      );
      setCourseData(response.data);
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    FetchData();
  }, [props]);

  //Scroll Keatas
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [props.id]);

  return (
    <Layout title="Course Menu">
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <img src="/images/Banner-SUV.png" />
        <Box sx={{ margin: "46px 70px 80px" }}>
          <DescriptionBox title={data.name} desc={data.description} />
        </Box>
      </Box>
      <Divider />
      <AnotherCourseList
        title="Another Favorite Course"
        courseData={courseData}
      />
    </Layout>
  );
}

export default CourseMenu;
