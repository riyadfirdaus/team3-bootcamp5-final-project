import { Box, Button, Divider, FormControl, Typography } from "@mui/material";
import dayjs from "dayjs";
import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import AddCartButton from "../../components/Button/AddCartButton";
import AnotherCourseList from "../../components/CourseList/AnotherCourseList";
import DateDropdown from "../../components/DateDropdown";
import DescriptionBox from "../../components/DescriptionBox";
import Layout from "../../components/Layout";
import { useSelector } from "react-redux";
import { selectAllCourses } from "../../Redux/CourseSlice";

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: "40px 70px 80px",
    gap: "40px",
  },
  cardForm: {
    display: "flex",
    flexFlow: { sm: "row", xs: "row wrap" },
    justifyContent: "center",
    gap: "40px",
  },
  image: {
    width: "400px",
    minWidth: "300px",
    height: "auto",

    alignSelf: "center",
    justifySelf: "center",
  },
  button: {
    maxWidth: { sm: "240px", xs: "100%" },
    height: "auto",
    flex: "auto",
    alignSelf: "center",
    justifySelf: "center",
  },
};

function CourseDetail() {
  const data = useLocation().state;
  // const today = dayjs();
  // const tomorrow = dayjs().add(1, "day");
  // const oneMonth = dayjs().add(31, "day");

  //Scroll Keatas
  const courseData = useSelector(selectAllCourses);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [data]);

  return (
    <Layout title="Course Detail">
      <Box sx={styles.container}>
        <Box sx={styles.cardForm}>
          <img src={data.image} style={styles.image} />
          <Box sx={{ flex: "auto" }}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "8px",
                margin: "0 0 32px",
              }}
            >
              <Typography sx={{ fontSize: 16 }} color="text.secondary">
                {data.type}
              </Typography>
              <Typography
                sx={{ fontSize: 24, fontWeight: "600" }}
                component="div"
              >
                {data.title}
              </Typography>
              <Typography
                sx={{ fontSize: 20, fontWeight: "600" }}
                color="#790B0A"
                component="div"
              >
                IDR {data.price}
              </Typography>
            </Box>

            {/* FORM */}
            <FormControl fullWidth>
              <DateDropdown />

              {/* <DatePicker
                label={"Select Schedule"}
                sx={{ maxWidth: 400 }}
                minDate={tomorrow}
                maxDate={oneMonth}
              /> */}
              <Box
                sx={{
                  display: "flex",
                  flexFlow: "row wrap",
                  minHeight: "40px",
                  gap: "16px",
                  marginTop: "60px",
                }}
              >
                <AddCartButton sx={styles.button} />
                <Button sx={styles.button} variant="contained">
                  Buy Now
                </Button>
              </Box>
            </FormControl>
          </Box>
        </Box>
        <DescriptionBox
          title="Description"
          desc={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          }
        />
      </Box>
      <Divider />
      <AnotherCourseList
        title="Another Favorite Course"
        courseData={courseData}
      />
    </Layout>
  );
}
export default CourseDetail;
