import { DeleteForever } from "@mui/icons-material";
import {
  Checkbox,
  Container,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
} from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CheckoutList from "../components/CheckoutList";
import Layout from "../components/Layout";
import PayNow from "../components/PayNow";
import PaymentMethod from "../modals/PaymentMethod";

export default function CheckOut() {
  const { currentUser } = useSelector((state) => state.auth);
  const [checked, setChecked] = useState([]);
  const [open, setOpen] = useState(false);
  const [total, setTotal] = useState(0);
  const [data, setData] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    getCart();
  }, [dispatch]);

  const getCart = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_BASE_API_URL}/api/User/GetUserCartItem?userId=${currentUser.id}`
      );
      const data = await response.data;
      setData(data);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteCart = async (cartId) => {
    try {
      await axios.delete(
        `${process.env.REACT_APP_BASE_API_URL}/api/User/DeleteCartItem?cartItemId=${cartId}`
      );
      getCart();
    } catch (error) {
      console.log(error);
    }
  };

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value.cartItemId);
    const newChecked = [...checked];
    if (currentIndex === -1) {
      newChecked.push(value.cartItemId);
      setTotal(total + value.bookingPrice);
    } else {
      newChecked.splice(currentIndex, 1);
      setTotal(total - value.bookingPrice);
    }
    setChecked(newChecked);
  };

  const togleSelectAll = () => {
    // check if all items are already checked
    const allChecked = data.every((data) => checked.includes(data.cartItemId));

    if (allChecked) {
      // if all items are already checked, uncheck all items
      setChecked([]);
      setTotal(0);
    } else {
      // if not all items are checked, check all items
      const newChecked = data.map((data) => data.cartItemId);
      const newTotal = data.reduce((acc, data) => {
        return acc + data.bookingPrice;
      }, 0);

      setChecked(newChecked);
      setTotal(newTotal);
    }
  };

  const handleDelete = (id) => {
    deleteCart(id);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <Layout title="Checkout">
      <Container maxWidth="xl">
        <PaymentMethod open={open} close={handleClose} checked={checked} />
        <List sx={{ marginBottom: 12 }}>
          <ListItem divider={true}>
            <Checkbox
              edge="start"
              checked={checked.length > 0}
              onChange={togleSelectAll}
              indeterminate={checked.length == data.length && data.length > 0}
            />
            <ListItemText primary="Pilih Semua" />
          </ListItem>
          {data.map((value, index) => {
            const labelId = `checkbox-list-secondary-label-${value}`;
            return (
              <ListItem
                key={index}
                divider={true}
                secondaryAction={
                  <ListItemButton
                    onClick={() => handleDelete(value.cartItemId)}
                  >
                    <DeleteForever />
                  </ListItemButton>
                }
              >
                <Checkbox
                  edge="start"
                  onChange={handleToggle(value)}
                  checked={checked.indexOf(value.cartItemId) !== -1}
                  inputProps={{ "aria-labelledby": labelId }}
                />
                <CheckoutList
                  img={{
                    url: value.image,
                    alt: value.brandName,
                  }}
                  type={value.typeName}
                  brand={value.brandName}
                  schedule={value.bookingDate}
                  price={value.bookingPrice}
                />
              </ListItem>
            );
          })}
        </List>
      </Container>
      {checked.length > 0 ? <PayNow payNow={handleOpen} total={total} /> : ""}
    </Layout>
  );
}
