import { Box, Button, TextField, Typography } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Navbar from "../components/Navbar";

//redux
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginFailed, loginStart, loginSuccess } from "../Redux/AuthSlice";

const styles = {
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    "@media (max-width:600px)": {
      flexDirection: "column",
      height: "unset",
    },
  },
  formContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#fff",
    padding: "24px",
    borderRadius: "8px",
    boxShadow: "0 4px 14px rgba(0,0,0,0.2)",
    maxWidth: "450px",
    width: "100%",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  input: {
    marginBottom: "16px",
  },
  button: {
    marginTop: "16px",
    width: "40%",
    alignSelf: "flex-end",
  },
  typographyHeader: {
    fontWeight: "bold",
    mb: 1,
  },
  typographySubHeader: {
    mb: 1,
  },
  typoUnder: {
    m: 3,
    color: "black",
    alignSelf: "center",
  },
  typoUnderSpan: {
    color: "#2F80ED",
    cursor: "pointer",
  },
  typoUnderLog: {
    m: 1,
    color: "black",
    alignSelf: "flex-start",
  },
};

const LoginRegister = ({ page }) => {
  const { currentUser } = useSelector((state) => state.auth);
  const base = process.env.REACT_APP_BASE_API_URL;
  const dispatch = useDispatch();
  console.log(base);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const navigate = useNavigate();
  const handleLogin = async (e) => {
    e.preventDefault();
    dispatch(loginStart());
    try {
      const response = await axios.post(`${base}/api/User/Login`, {
        email,
        password,
      });
      const data = response.data;
      if (data?.status) {
        dispatch(loginSuccess(data));
        toast("Login Succed");
        setEmail("");
        setPassword("");
        navigate("/");
      } else {
        toast("Please Activate Your Accont");
      }
    } catch (error) {
      dispatch(loginFailed(error.response.data));
      toast(error.response.data);
    }
  };
  const checkInput = (inp) => {
    if (inp.trim().length !== 0) {
      return true;
    } else {
      return false;
    }
  };

  const register = async (e) => {
    e.preventDefault();
    try {
      if (password !== confirmPassword) {
        return toast("Password and Confirm Password doesn't Match");
      } else if (
        checkInput(name) === false ||
        checkInput(email) === false ||
        checkInput(password) === false
      ) {
        return toast("Please input all the necesary field");
      } else {
        const resp = await axios.post(`${base}/api/User/Register`, {
          username: name,
          password: password,
          email: email,
          role: false,
          status: false,
        });
        console.log(resp);
        setName("");
        setEmail("");
        setPassword("");
        setConfirmPassword("");
        navigate("/successemail");
      }
    } catch (error) {
      dispatch(loginFailed(error.response.data));
      toast(error.response.data);
    }
  };

  const redir = () => {
    if (currentUser !== null && currentUser.status) {
      navigate("/");
    }
  };

  useEffect(() => {
    redir();
  }, [redir]);

  return (
    <>
      <Navbar />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      {page === "register" ? (
        <Box sx={styles.root}>
          <Box sx={styles.formContainer} component="form">
            <Typography
              alignSelf="start"
              variant="h5"
              color="primary"
              sx={styles.typographyHeader}
            >
              Lets Join our couse!
            </Typography>
            <Typography
              alignSelf="start"
              variant="h6"
              color="primary"
              sx={styles.typographySubHeader}
            >
              Please register first
            </Typography>
            <Box sx={styles.form}>
              <TextField
                type="text"
                required
                label="Full Name"
                variant="outlined"
                sx={styles.input}
                onChange={(e) => setName(e.target.value)}
              />
              <TextField
                label="Email"
                type="email"
                required
                variant="outlined"
                sx={styles.input}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                label="Password"
                type="password"
                required
                variant="outlined"
                sx={styles.input}
                onChange={(e) => setPassword(e.target.value)}
              />
              <TextField
                label="Confirm Password"
                type="password"
                required
                variant="outlined"
                sx={styles.input}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <Button
                onClick={register}
                variant="contained"
                color="primary"
                sx={styles.button}
              >
                Register
              </Button>
              <Typography sx={styles.typoUnder}>
                Have account?
                <Typography
                  sx={styles.typoUnderSpan}
                  component="span"
                  onClick={() => navigate("/login")}
                >
                  {" "}
                  Login here
                </Typography>
              </Typography>
            </Box>
          </Box>
        </Box>
      ) : (
        <Box sx={styles.root}>
          <Box sx={styles.formContainer}>
            <Typography
              alignSelf="start"
              variant="h5"
              color="primary"
              sx={styles.typographyHeader}
            >
              Welcome Back!
            </Typography>
            <Typography
              alignSelf="start"
              variant="h6"
              color="primary"
              sx={styles.typographySubHeader}
            >
              Please login first
            </Typography>
            <Box sx={styles.form}>
              <TextField
                label="Email"
                variant="outlined"
                sx={styles.input}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                label="Password"
                variant="outlined"
                type="password"
                sx={styles.input}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Typography sx={styles.typoUnderLog}>
                Forgot Password?
                <Typography
                  sx={styles.typoUnderSpan}
                  component="span"
                  onClick={() => navigate("/resetpassemail")}
                >
                  {" "}
                  Click Here
                </Typography>
              </Typography>
              <Button
                variant="contained"
                color="primary"
                sx={styles.button}
                onClick={handleLogin}
              >
                Login
              </Button>
              <Typography sx={styles.typoUnder}>
                Dont have account?
                <Typography
                  sx={styles.typoUnderSpan}
                  component="span"
                  onClick={() => navigate("/register")}
                >
                  {" "}
                  Sign Up here
                </Typography>
              </Typography>
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
};

export default LoginRegister;
