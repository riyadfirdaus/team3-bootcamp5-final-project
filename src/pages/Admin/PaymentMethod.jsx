import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchpayments, selectAllPayments } from "../../Redux/PaymentSlice";
import EnhancedTable from "../../components/Admin/PaymentMethod/EnhancedTable";

function PaymentMethod() {
  const dispatch = useDispatch();
  const paymentMethod = useSelector(selectAllPayments);

  useEffect(() => {
    dispatch(fetchpayments());
  }, [dispatch]);

  return <EnhancedTable tableData={paymentMethod} />;
}

export default PaymentMethod;
