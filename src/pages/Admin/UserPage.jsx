import { Button, Typography } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { UpdateUser } from "../../modals/UpdateUser";

function UserPage() {
  const { currentUser } = useSelector((state) => state.auth);
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState({
    username: "",
    email: "",
    role: true,
  });

  const getUsers = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/Users`,
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
          },
        }
      );
      const data = response.data;
      setData(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleTogleStatus = async (userId, email) => {
    try {
      await axios.put(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/TogleStatusUser?id=${userId}&email=${email}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
            ContentType: "application/json",
          },
        }
      );
      getUsers();
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = (user) => {
    setOpen(true);
    setValue(user);
  };

  return (
    <>
      <Typography variant="h4" gutterBottom sx={{ marginTop: "16px" }}>
        User Data
      </Typography>
      <UpdateUser
        open={open}
        close={handleClose}
        user={value}
        getUsers={getUsers}
      />
      <TableContainer sx={{ marginBottom: 20 }}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>No</TableCell>
              <TableCell align="center">Username</TableCell>
              <TableCell align="center">Email</TableCell>
              <TableCell align="center">Role</TableCell>
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((user, index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {index + 1}
                </TableCell>
                <TableCell align="center">{user.username}</TableCell>
                <TableCell align="center">{user.email}</TableCell>
                <TableCell align="center">
                  {user.role ? "Admin" : "Basic"}
                </TableCell>
                <TableCell align="center">
                  <Button
                    variant={user.status ? "contained" : "outlined"}
                    onClick={() => handleTogleStatus(user.id, user.email)}
                  >
                    {user.status ? "Actived" : "Deactived"}
                  </Button>
                </TableCell>
                <TableCell align="center">
                  <Button variant="outlined" onClick={() => handleOpen(user)}>
                    Edit
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

export default UserPage;
