import { Button, Typography } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import axios from "axios";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function InvoicePage() {
  const { currentUser } = useSelector((state) => state.auth);
  const [data, setData] = useState([]);
  const getUsers = async () => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/GetAllInvoice`,
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
          },
        }
      );
      const data = response.data;
      setData(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);
  return (
    <>
      <Typography variant="h4" gutterBottom sx={{ marginTop: "16px" }}>
        Invoice Data
      </Typography>
      <TableContainer sx={{ marginBottom: 20 }}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>No</TableCell>
              <TableCell align="center">Invoice Id</TableCell>
              <TableCell align="center">Order Date</TableCell>
              <TableCell align="center">Total Course</TableCell>
              <TableCell align="center">Total Price</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.map((user, index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {index + 1}
                </TableCell>
                <TableCell align="center">{user?.invoiceId}</TableCell>
                <TableCell align="center">
                  {dayjs(user?.orderDate?.schedule).format(
                    "dddd, DD MMMM YYYY"
                  )}
                </TableCell>
                <TableCell align="center">{user?.totalCourse}</TableCell>
                <TableCell align="center">{user?.totalPrice}</TableCell>
                <TableCell align="center">
                  <Button
                    variant="outlined"
                    component={Link}
                    to={`/invoice/${user?.invoiceId}`}
                  >
                    view
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

export default InvoicePage;
