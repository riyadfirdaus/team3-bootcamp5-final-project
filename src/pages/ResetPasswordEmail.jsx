import { Typography, Box, Container, TextField, Button } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import { passwordResetStart, passwordResetSuccess } from "../Redux/AuthSlice";

const styles = {
  container: {
    width: "70%",
    marginTop: "10%",
  },
  typoemailAtas: {
    mb: 1,
    fontSize: { sm: "45px", xs: "25px" },
  },
  typoemailBawah: {
    mb: 3,
    fontSize: { sm: "35px", xs: "15px" },
  },
  buttonBox: {
    display: "flex",
    justifyContent: "flex-end",
    gap: "20px",
    mt: 3,
  },
};
const ResetPasswordEmail = ({ page }) => {
  const { resetData } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const baseUrl = process.env.REACT_APP_BASE_API_URL;
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [pass, setNewpass] = useState("");
  const [confirmPass, setConfirmNewpass] = useState("");

  const handleSendEmail = async (e) => {
    e.preventDefault();
    toast("Sending Email");
    try {
      toast("Pease Check Your Email");
      const resp = await axios.post(`${baseUrl}/api/User/EmailResetPassword`, {
        email,
      });
      dispatch(passwordResetStart(resp?.data));
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  const checkInput = (inp) => {
    if (inp.trim().length !== 0) {
      return true;
    } else {
      return false;
    }
  };

  const handleReset = async (e) => {
    e.preventDefault();

    try {
      if (pass !== confirmPass) {
        return toast("Password and Confirm Password doesn't Match");
      } else if (
        checkInput(pass) === false ||
        checkInput(confirmPass) === false
      ) {
        return toast("Please input all the necesary field");
      } else {
        const resp = await axios.post(`${baseUrl}/api/User/ResetPassword`, {
          password: pass,
          email: resetData.email,
        });
        setNewpass("");
        setConfirmNewpass("");
        navigate("/login");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Navbar />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      {page === "email" ? (
        <Container sx={styles.container}>
          <Box>
            <Typography sx={styles.typoemailAtas}>Reset Password</Typography>
            <Typography sx={styles.typoemailBawah}>
              Send OTP code to your email address
            </Typography>
          </Box>
          <Box component="form">
            <TextField
              label="Email"
              variant="outlined"
              fullWidth
              onChange={(e) => setEmail(e.target.value)}
            />
            <Box fullwidth sx={styles.buttonBox}>
              <Button
                variant="outlined"
                color="primary"
                onClick={() => navigate("/")}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSendEmail}
              >
                Confirm
              </Button>
            </Box>
          </Box>
        </Container>
      ) : (
        <Container sx={styles.container}>
          <Box>
            <Typography sx={styles.typoemailAtas}>Create Password</Typography>
          </Box>
          <Box component="form">
            <TextField
              type="password"
              label="New Password"
              variant="outlined"
              fullWidth
              sx={{ mb: 1 }}
              onChange={(e) => setNewpass(e.target.value)}
            />
            <TextField
              type="password"
              label="Confir Password"
              variant="outlined"
              fullWidth
              onChange={(e) => setConfirmNewpass(e.target.value)}
            />
            <Box fullwidth sx={styles.buttonBox}>
              <Button
                variant="outlined"
                color="primary"
                onClick={() => navigate("/")}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" onClick={handleReset}>
                Confirm
              </Button>
            </Box>
          </Box>
        </Container>
      )}
    </>
  );
};

export default ResetPasswordEmail;
