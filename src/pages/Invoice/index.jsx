import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import {
  Breadcrumbs,
  Button,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { fetchInvoices } from "../../Redux/InvoiceSlice";
import Layout from "../../components/Layout";
import { TableDate } from "../../components/TableDate";

export default function Invoice() {
  const { currentUser } = useSelector((state) => state.auth);
  const { invoiceData } = useSelector((state) => state.invoice);
  const dispatch = useDispatch();
  const breadcrumbs = [
    <Typography
      component={Link}
      sx={{ textDecoration: "none" }}
      fontWeight={600}
      key="1"
      color="secondary"
      to="/"
    >
      Home
    </Typography>,
    <Typography key="2" fontWeight={600} color="primary">
      Invoice
    </Typography>,
  ];

  useEffect(() => {
    dispatch(fetchInvoices(currentUser.id));
  }, [dispatch]);


  return (
    <Layout title="Invoice">
      <Container maxWidth="xl">
        <Stack spacing={2} paddingY={3}>
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            {breadcrumbs}
          </Breadcrumbs>
        </Stack>
        <Typography
          variant="h6"
          fontWeight={600}
          color="secondary"
          paddingBottom={2}
        >
          Menu Invoice
        </Typography>
        <TableContainer sx={{ marginBottom: 20 }}>
          <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>No</TableCell>
                <TableCell align="center">No. Invoice</TableCell>
                <TableCell align="center">Date</TableCell>
                <TableCell align="center">Total Course</TableCell>
                <TableCell align="center">Total Price</TableCell>
                <TableCell align="center">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {invoiceData.map((invoice, index) => (
                <TableRow key={index}>
                  <TableCell component="th" scope="row">
                    {index + 1}
                  </TableCell>
                  <TableCell align="center">{invoice.invoiceId}</TableCell>
                  <TableDate date={invoice.orderDate} />
                  <TableCell align="center">{invoice.totalCourse}</TableCell>
                  <TableCell align="center">
                    IDR {invoice.totalPrice.toLocaleString("id-ID")}
                  </TableCell>
                  <TableCell align="center">
                    <Button
                      variant="outlined"
                      component={Link}
                      to={invoice.invoiceId}
                    >
                      Details
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    </Layout>
  );
}
