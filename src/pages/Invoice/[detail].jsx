import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import { Box, Breadcrumbs, Container, Stack, Typography } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { fetchInvoiceDetail } from "../../Redux/InvoiceSlice";
import Layout from "../../components/Layout";
import { TableDate } from "../../components/TableDate";

export default function InvoiceDetails() {
  const { id } = useParams();
  const { invoiceDataDetail } = useSelector((state) => state.invoice);
  const [total, setTotal] = useState(0);
  const dispatch = useDispatch();
  const breadcrumbs = [
    <Typography
      component={Link}
      sx={{ textDecoration: "none" }}
      fontWeight={600}
      key="1"
      color="secondary"
      to="/"
    >
      Home
    </Typography>,
    <Typography
      component={Link}
      sx={{ textDecoration: "none" }}
      fontWeight={600}
      key="2"
      color="secondary"
      to="/invoice"
    >
      Invoice
    </Typography>,
    <Typography key="3" fontWeight={600} color="primary">
      Details Invoice
    </Typography>,
  ];

  useEffect(() => {
    dispatch(fetchInvoiceDetail(id));
  }, [dispatch]);

  useEffect(() => {
    setTotal(
      invoiceDataDetail.reduce((acc, invoice) => {
        return acc + invoice.bookingPrice;
      }, 0)
    );
  }, [invoiceDataDetail]);

  return (
    <Layout title="Details Invoice">
      <Container maxWidth="xl">
        <Stack spacing={2} paddingY={3}>
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            {breadcrumbs}
          </Breadcrumbs>
        </Stack>
        <Typography
          variant="h6"
          fontWeight={600}
          color="secondary"
          paddingBottom={2}
        >
          Details Invoice
        </Typography>
        <Box
          sx={{
            marginBottom: 2,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "end",
            flexWrap: "wrap",
          }}
        >
          <Box>
            <Typography color={"secondary"}>No. Invoice : {id}</Typography>
            <Typography color={"secondary"}>Date : 12 June 2022</Typography>
          </Box>
          <Box display="flex" gap={2}>
            <Typography color={"secondary"} fontWeight={600}>
              Total Price
            </Typography>
            <Typography color={"secondary"} fontWeight={600}>
              IDR {total.toLocaleString("id-ID")}
            </Typography>
          </Box>
        </Box>
        <TableContainer sx={{ marginBottom: 20 }}>
          <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>No</TableCell>
                <TableCell align="center">Course Name</TableCell>
                <TableCell align="center">Type</TableCell>
                <TableCell align="center">Schedule</TableCell>
                <TableCell align="center">Price</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {invoiceDataDetail.map((data, index) => (
                <TableRow key={index}>
                  <TableCell component="th" scope="row">
                    {index + 1}
                  </TableCell>
                  <TableCell align="center">{data.courseName}</TableCell>
                  <TableCell align="center">{data.courseType}</TableCell>
                  <TableDate date={data.bookingDate} />
                  <TableCell align="center">
                    IDR {data.bookingPrice.toLocaleString("id-ID")}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    </Layout>
  );
}
