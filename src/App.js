import { ThemeProvider } from "@emotion/react";
import { createTheme } from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import "./App.css";
import { UserContext } from "./context/UserContext";
import MainRoutes from "./routes/MainRoutes";

const theme = createTheme({
  palette: {
    primary: {
      main: "#790B0A",
    },
    secondary: {
      main: "#4F4F4F",
    },
    appbar: {
      main: "#fffff",
    },
  },
  typography: {
    fontFamily: ["Montserrat"],
  },
  components: {
    MuiTableCell: {
      styleOverrides: {
        head: {
          backgroundColor: "#790B0A",
          color: "#FFFFFF",
          fontWeight: 600,
        },
      },
    },
    MuiTableRow: {
      styleOverrides: {
        root: {
          "& .MuiTableCell-body": {
            color: "#4F4F4F",
          },
          "&:nth-of-type(even)": {
            backgroundColor: "#790B0A1A ",
          },
          // hide last border
          "&:last-child td, &:last-child th": {
            border: 0,
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: "capitalize", // button text capitalize
        },
      },
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <UserContext>
          <MainRoutes />
        </UserContext>
      </LocalizationProvider>
    </ThemeProvider>
  );
}

export default App;
