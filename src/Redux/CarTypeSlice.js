import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from "@reduxjs/toolkit";
import axios from "axios";

const carTypesAdapter = createEntityAdapter({
  selectId: (carType) => carType.id,
});

const initialState = carTypesAdapter.getInitialState({
  status: "idle",
  error: null,
});

export const fetchCarTypes = createAsyncThunk(
  "carType/fetchCarTypes",
  async () => {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_API_URL}/api/User/GetAllType`
    );
    return response.data;
  }
);

export const carTypeSlice = createSlice({
  name: "carType",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCarTypes.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchCarTypes.fulfilled, (state, action) => {
        state.status = "succeeded";
        // normalize the data and add it to the state
        carTypesAdapter.setAll(state, action.payload);
      })
      .addCase(fetchCarTypes.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export const { selectAll: selectAllTypes, selectById: selectTypesById } =
  carTypesAdapter.getSelectors((state) => state.carType);

export default carTypeSlice.reducer;
