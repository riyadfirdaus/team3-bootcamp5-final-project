import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from "@reduxjs/toolkit";
import axios from "axios";
import { useSelector } from "react-redux";

const paymentsAdapter = createEntityAdapter({
  selectId: (payment) => payment.id,
});

const initialState = paymentsAdapter.getInitialState({
  status: "idle",
  error: null,
});

export const fetchpayments = createAsyncThunk(
  "payment/fetchpayments",
  async (_, { getState }) => {
    const { auth } = getState();
    const token = auth.currentUser.token;
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_API_URL}/api/Admin/PaymentMethods`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  }
);

export const paymentSlice = createSlice({
  name: "payment",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchpayments.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchpayments.fulfilled, (state, action) => {
        state.status = "succeeded";
        paymentsAdapter.setAll(state, action.payload);
      })
      .addCase(fetchpayments.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export const { selectAll: selectAllPayments, selectById: selectPaymentById } =
  paymentsAdapter.getSelectors((state) => state.payment);

export default paymentSlice.reducer;
