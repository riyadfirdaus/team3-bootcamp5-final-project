import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  invoiceData: [],
  invoiceDataDetail: [],
  loading: false,
  error: null,
};

export const fetchInvoices = createAsyncThunk(
  "invoice/Invoices",
  async (userId) => {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_API_URL}/api/User/GetUserInvoices?userId=${userId}`
    );
    return response.data;
  }
);

export const fetchInvoiceDetail = createAsyncThunk(
  "invoice/InvoiceDetail",
  async (invoiceId) => {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_API_URL}/api/User/GetInvoiceItems?invoiceId=${invoiceId}`
    );
    return response.data;
  }
);

export const insertInvoice = createAsyncThunk(
  "invoice/InsertInvoic",
  async ({ userId, cartItemIds }) => {
    const response = await axios.post(
      `${process.env.REACT_APP_BASE_API_URL}/api/User/CreateInvoice?userId=${userId}`,
      { cartItemIds }
    );
    console.log(cartItemIds);
    return response.data;
  }
);

export const invoiceSlice = createSlice({
  name: "invoice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // Fetch Invoice Datas
      .addCase(fetchInvoices.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchInvoices.fulfilled, (state, action) => {
        (state.loading = false),
          (state.error = false),
          (state.invoiceData = action.payload);
      })
      .addCase(fetchInvoices.rejected, (state, action) => {
        (state.loading = false),
          (state.error = true),
          (state.error = action.error.message);
      })
      // Fetch Invoice Data Detail
      .addCase(fetchInvoiceDetail.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchInvoiceDetail.fulfilled, (state, action) => {
        (state.loading = false),
          (state.error = false),
          (state.invoiceDataDetail = action.payload);
      })
      .addCase(fetchInvoiceDetail.rejected, (state, action) => {
        (state.loading = false),
          (state.error = true),
          (state.error = action.error.message);
      })
      // Insert Invoice
      .addCase(insertInvoice.pending, (state) => {
        state.loading = true;
      })
      .addCase(insertInvoice.fulfilled, (state, action) => {
        (state.loading = false),
          (state.error = false),
          (state.insertInvoice = action.payload);
      })
      .addCase(insertInvoice.rejected, (state, action) => {
        (state.loading = false),
          (state.error = true),
          (state.error = action.error.message);
      });
  },
});

export default invoiceSlice.reducer;
