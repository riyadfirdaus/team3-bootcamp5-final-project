import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    currentUser : null,
    resetData: null,
    loading : false,
    error: false
}

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        loginStart: (state ) => {
            state.loading = true
        },
        loginSuccess: (state, action) => {
            state.loading = false,
            state.error = false,
            state.currentUser = action.payload
        },
        loginFailed: (state) => {
            state.loading = false,
            state.error = true
        },
        logOut: (state) => {
            state.currentUser = null,
            state.loading = false,
            state.error = false
        },
        passwordResetStart: (state, action) => {
            state.resetData = action.payload
        },
        passwordResetSuccess: (state) =>{
            state.resetData = null
        }
    }
})

export const { loginStart, loginSuccess, loginFailed, logOut, passwordResetStart, passwordResetSuccess } = authSlice.actions
export default authSlice.reducer