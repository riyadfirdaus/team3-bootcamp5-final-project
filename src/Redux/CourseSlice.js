import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import axios from "axios";

const coursesAdapter = createEntityAdapter({
  selectId: (course) => course.id,
});

const initialState = coursesAdapter.getInitialState({
  status: "idle",
  error: null,
});

export const fetchCourses = createAsyncThunk(
  "course/fetchCourses",
  async () => {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_API_URL}/api/User/GetAllCourse`
    );
    return response.data;
  }
);

export const courseSlice = createSlice({
  name: "course",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCourses.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchCourses.fulfilled, (state, action) => {
        state.status = "succeeded";
        coursesAdapter.setAll(state, action.payload);
      })
      .addCase(fetchCourses.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export const { selectAll: selectAllCourses, selectById: selectCourseById } =
  coursesAdapter.getSelectors((state) => state.course);

export default courseSlice.reducer;
