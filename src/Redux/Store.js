import { combineReducers, configureStore } from "@reduxjs/toolkit";
import authReducer from "./AuthSlice";
import courseReducer from "./CourseSlice";
import invoiceReducer from "./InvoiceSlice";
import carTypeReducer from "./CarTypeSlice";
import paymentReducer from "./PaymentSlice";

import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
  persistReducer,
  persistStore,
} from "redux-persist";

import storage from "redux-persist/lib/storage";
import { PersistGate } from "redux-persist/integration/react";
import { useDispatch } from "react-redux";
import { useEffect } from "react";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
};

const reducers = {
  auth: authReducer,
  payment: paymentReducer,
  invoice: invoiceReducer,
  course: courseReducer,
  carType: carTypeReducer,
};

const rootReducer = combineReducers(reducers);
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);
