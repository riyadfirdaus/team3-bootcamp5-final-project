import { Payment } from "@mui/icons-material";
import {
  Box,
  Button,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Modal,
  Typography,
} from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { insertInvoice } from "../Redux/InvoiceSlice";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "80%",
  bgcolor: "background.paper",
  boxShadow: 24,
  borderRadius: 2,
  p: 3,
};

export default function PaymentMethod(props) {
  const { open, close, checked } = props;
  const { currentUser } = useSelector((state) => state.auth);
  const [selectedPayment, setSelectedPayment] = useState("");
  const [paymentMethods, setPaymentMethods] = useState([]);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userId = currentUser.id;
  const cartItemIds = checked;

  const handlePayNowClick = async (e) => {
    e.preventDefault();
    // lakukan proses pembayaran dengan nilai selectedPayment
    selectedPayment
      ? (await dispatch(insertInvoice({ userId, cartItemIds })),
        navigate("/successpurchase"))
      : toast("Payment Method Not Selected");
  };

  const getMethods = async () => {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_API_URL}/api/User/PaymentMethods`
    );
    const data = await response.data;
    setPaymentMethods(data);
  };

  useEffect(() => {
    getMethods();
  }, []);

  console.log(paymentMethods);

  return (
    <Modal open={open} onClose={close}>
      <Box sx={{ ...style, maxWidth: 326 }} aria-labelledby="child-modal-title">
        <Typography
          id="child-modal-title"
          sx={{
            textAlign: "center",
            fontSize: "20px",
            fontWeight: 500,
            marginBottom: 2,
          }}
        >
          Select Payment Method
        </Typography>
        <List>
          {paymentMethods.map((option) => (
            <ListItemButton
              key={option.id}
              selected={selectedPayment === option.name}
              onClick={() => setSelectedPayment(option.name)}
            >
              <ListItemIcon>
                <img src={option.logo} alt={option.name} loading="lazy" />
              </ListItemIcon>
              <ListItemText primary={option.name} />
            </ListItemButton>
          ))}
        </List>
        <Box
          sx={{ marginTop: 2, display: "flex", justifyContent: "end", gap: 2 }}
        >
          <Button onClick={close} size="large" variant="outlined">
            Cancel
          </Button>
          <Button onClick={handlePayNowClick} size="large" variant="contained">
            Pay now
          </Button>
        </Box>
      </Box>
    </Modal>
  );
}
