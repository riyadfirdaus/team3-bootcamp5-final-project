import {
  Box,
  Button,
  FormControlLabel,
  List,
  MenuItem,
  Modal,
  Radio,
  RadioGroup,
  Select,
  SliderValueLabel,
  TextField,
  Typography,
} from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const styles = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "80%",
  bgcolor: "background.paper",
  boxShadow: 24,
  borderRadius: 2,
  p: 3,
  input: {
    width: "100%",
  },
};

export const UpdateUser = (props) => {
  const { open, close, user, getUsers } = props;
  const { currentUser } = useSelector((state) => state.auth);
  const [data, setData] = useState({
    name: "",
    email: "",
    role: false,
  });

  const handleUserUpdate = async (userId, email) => {
    try {
      const newData = {
        username: data.name,
        email: data.email,
        role: JSON.parse(data.role),
      };
      await axios.put(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/UpdateUser?id=${userId}&email=${email}`,
        newData,
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
            ContentType: "application/json",
          },
        }
      );
      getUsers();
      close();
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    setData({
      name: user.username,
      email: user.email,
      role: user.role,
    });
  }, [user]);

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setData({ ...data, [name]: value });
  };

  return (
    <Modal open={open} onClose={close}>
      <Box
        sx={{ ...styles, maxWidth: 326 }}
        aria-labelledby="child-modal-title"
      >
        <Typography
          id="child-modal-title"
          sx={{
            textAlign: "center",
            fontSize: "20px",
            fontWeight: 500,
            marginBottom: 2,
          }}
        >
          Update User
        </Typography>
        <List>
          <TextField
            type="text"
            name="name"
            value={data.name}
            variant="outlined"
            sx={styles.input}
            onChange={handleChange}
          />
          <TextField
            type="email"
            name="email"
            value={data.email}
            variant="outlined"
            sx={styles.input}
            onChange={handleChange}
          />

          <RadioGroup
            aria-label="role"
            name="role"
            value={data.role ?? user.role.toString()}
            onChange={handleChange}
          >
            <FormControlLabel value="true" control={<Radio />} label="Admin" />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="Basic User"
            />
          </RadioGroup>
        </List>
        <Box
          sx={{ marginTop: 2, display: "flex", justifyContent: "end", gap: 2 }}
        >
          <Button
            onClick={() => handleUserUpdate(user.id, user.email)}
            size="large"
            variant="contained"
          >
            Update
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};
