import React, { createContext, useState } from "react";

export const AuthContext = createContext()

export const UserContext = ({children}) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const setLogin = (state) =>{
      setIsLoggedIn(state)
    }
    const setLogOut = () =>{
      setIsLoggedIn(false)
    }
  return (
    <AuthContext.Provider value={{isLoggedIn, setLogin}}>
        {children}
    </AuthContext.Provider>
  );
};


