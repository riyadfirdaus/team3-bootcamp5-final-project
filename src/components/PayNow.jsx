import React from "react";
import { Box, Button, Container, Typography } from "@mui/material";

export default function PayNow(props) {
  const { payNow, total } = props
  return (
    <Box
      sx={{
        position: "fixed",
        bottom: 0,
        width: "100%",
        bgcolor: "background.paper",
        boxShadow: 10,
      }}
    >
      <Container
        maxWidth="xl"
        sx={{
          display: "flex",
          justifyContent: "space-between",
          p: 3,
          flexWrap: "wrap",
          alignItems: "center",
          gap: 2,
        }}
      >
        <Box display="flex" alignItems="center" gap={2}>
          Total Price
          <Typography
            color="primary"
            sx={{ fontSize: 24, fontWeight: "600" }}
          >
            IDR {total.toLocaleString('id-ID')}
          </Typography>
        </Box>
        <Button
          variant="contained"
          color="primary"
          sx={{ padding: "8px 80px" }}
          onClick={payNow}
        >
          Pay Now
        </Button>
      </Container>
    </Box>
  );
}
