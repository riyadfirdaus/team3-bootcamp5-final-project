import { Button } from "@mui/material";
import axios from "axios";
import React from "react";

function AddCartButton() {
  const handleSubmit = () => {
    axios
      .post("/api/form", {
        firstName: "John",
        lastName: "Doe",
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <Button
      onClick={handleSubmit}
      sx={{
        maxWidth: { sm: "240px", xs: "100%" },
        height: "auto",
        flex: "auto",
        alignSelf: "center",
        justifySelf: "center",
      }}
      variant="outlined"
    >
      Add To Cart
    </Button>
  );
}

export default AddCartButton;
