import axios from "axios";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchpayments } from "../../Redux/PaymentSlice";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

function DeleteButton({ selectedItem, buttonColor, handleClick }) {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state) => state.auth);
  async function deleteAsync(id) {
    try {
      const response = await axios.delete(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/PaymentMethods/?Id=${id}`,
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
          },
        }
      );

      dispatch(fetchpayments());
      console.log(response.data);
    } catch (error) {
      console.log(error.response);
    }
  }

  async function handleDelete(event) {
    handleClick(event, selectedItem);
    event.preventDefault();
    for (const item of selectedItem) {
      await deleteAsync(item.id);
      dispatch(fetchpayments());
    }
  }
  return (
    <IconButton onClick={handleDelete}>
      <DeleteIcon style={{ color: buttonColor }} />
    </IconButton>
  );
}

export default DeleteButton;
