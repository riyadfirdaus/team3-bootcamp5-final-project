import { Logout } from "@mui/icons-material";
import MenuIcon from "@mui/icons-material/Menu";
import PersonIcon from "@mui/icons-material/Person";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Toolbar from "@mui/material/Toolbar";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import logo from "../assets/logo.png";
//redux
import { useDispatch, useSelector } from "react-redux";
import { logOut } from "../Redux/AuthSlice";



const Navbar = () => {
  const pages = [{name: "My Class", link: '/myclass'}, {name: "Invoice", link: '/invoice'}];
  const settings = [{name: "Dashboard", link:'/admin/*'}];
  const { currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const loggedIn = () => {
    if (currentUser && currentUser.status) {
      return true;
    } else {
      return false;
    }
  };
  const navigate = useNavigate();
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const handleLogout = () => {
    dispatch(logOut());
    navigate("/");
  };

  const handleAdmin = () => {
    if(currentUser.role){
      handleCloseNavMenu()
    navigate('/admin/')
    }
  }

  useEffect(() => {
    loggedIn();
  }, [currentUser]);
  return (
    <AppBar position="sticky" color="appbar" id="navbar">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ display: { xs: "none", md: "flex" }, mr: 1 }}>
            <img src={logo} />
          </Box>
          <Typography
            variant="h6"
            noWrap
            sx={{
              mr: 2,
              display: { xs: "none", md: "flex" },
              fontSize: "24px",
              fontFamily: "Montserrat",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
              cursor: "pointer",
            }}
            onClick={() => navigate("/")}
          >
            OTOMOBIL
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page.name} onClick={() => navigate(`${page.link}`)}>
                  <Typography
                    textAlign="center"
                    sx={{ color: "black" }}
                    style={{ color: "#790B0A" }}
                  >
                    {page.name}
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          <Box sx={{ display: { xs: "flex", md: "none" }, mr: 1 }}>
            <img src={logo} />
          </Box>
          <Typography
            variant="h5"
            noWrap
            sx={{
              mr: 2,
              display: { xs: "flex", md: "none" },
              flexGrow: 1,
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
            }}
            onClick={() => navigate("/")}
          >
            Otomobil
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}></Box>

          <Box sx={{ flexGrow: 0, flexDirection: "row" }}>
            {currentUser !== null ? (
              <Box sx={{ display: { md: "flex" } }} style={{ gap: "20px" }}>
                <Box
                  sx={{ display: { xs: "none", md: "flex" } }}
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    gap: "20px",
                  }}
                >
                  <IconButton
                    sx={{ my: 2, display: "block" }}
                    style={{ color: "#790B0A" }}
                    onClick={() => navigate(`${'/checkout'}`)}
                  >
                    <ShoppingCartIcon fontSize="small" />
                  </IconButton>
                  {pages.map((page) => (
                    <Button
                      key={page.name}
                      onClick={() => navigate(`${page.link}`)}
                      sx={{ my: 2, display: "block" }}
                      style={{ color: "#790B0A" }}
                    >
                      {page.name}
                    </Button>
                  ))}
                  <Box
                    style={{
                      backgroundColor: "#790B0A",
                      width: "2px",
                      height: "60%",
                    }}
                  ></Box>
                </Box>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleAdmin} sx={{ p: 0, mr: 2 }}>
                    <PersonIcon style={{ color: "#790B0A" }} />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleLogout} sx={{ p: 0 }}>
                    <Logout
                      style={{ color: "#790B0A" }}
                      sx={{ display: { xs: "none", md: "flex" } }}
                    />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  {settings.map((setting) => (
                    <MenuItem key={setting} onClick={handleAdmin}>
                      <Typography textAlign="center">{setting.name}</Typography>
                    </MenuItem>
                  ))}
                </Menu>
              </Box>
            ) : (
              <Box style={{ display: "flex", gap: "10px" }}>
                <Button
                  sx={{ display: { xs: "none", sm: "block" } }}
                  onClick={() => navigate("/register")}
                >
                  Sign Up
                </Button>
                <Button
                  sx={{ display: { xs: "none", sm: "block" } }}
                  style={{ backgroundColor: "#790B0A", color: "white" }}
                  onClick={() => navigate("/login")}
                >
                  Login
                </Button>
                <IconButton
                  onClick={() => navigate("/login")}
                  sx={{
                    p: 0,
                    mr: 2,
                    display: !loggedIn ? { xs: "block", sm: "none" } : "none",
                  }}
                >
                  <PersonIcon style={{ color: "#790B0A" }} />
                </IconButton>
              </Box>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Navbar;
