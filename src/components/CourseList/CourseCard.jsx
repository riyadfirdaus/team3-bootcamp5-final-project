import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { Link } from "react-router-dom";

function CourseCard({ id, type, title, price, url }) {
  const path = `/coursedetail/${id}`;
  return (
    <div>
      <Card sx={{ width: 350 }}>
        <CardActionArea
          component={Link}
          to={{
            pathname: path,
          }}
          state={{
            id: id,
            type: type,
            title: title,
            price: price,
            image: url,
          }}
        >
          <CardMedia component="img" width="350" image={url} alt={title} />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "4px",
                height: "94px",
              }}
            >
              <Typography sx={{ fontSize: 14 }} color="text.secondary">
                {type}
              </Typography>
              <Typography
                sx={{ fontSize: 16, fontWeight: "600" }}
                gutterBottom
                component="div"
              >
                {title}
              </Typography>
            </Box>
            <Typography
              sx={{ fontSize: 20, fontWeight: "600" }}
              color="#790B0A"
              gutterBottom
              component="div"
            >
              IDR {price}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
}

export default CourseCard;
