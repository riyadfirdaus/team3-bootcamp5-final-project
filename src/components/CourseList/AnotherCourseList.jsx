import { Box, Typography } from "@mui/material";
import React from "react";
import CourseCard from "./CourseCard";

let styles = {
  container: {
    display: "flex",
    flexFlow: "column",
    gap: "80px",
    justifyContent: "center",
    alignItems: "center",
    margin: { md: "64px 80px", xs: "64px 24px" },
  },

  cardList: {
    display: "flex",
    flexFlow: "row wrap",
    gap: "24px",
    justifyContent: "center",
  },
};

function createList(data) {
  if (data.length === 0) {
    return <Typography>Course is not currently available</Typography>;
  }
  return data.map((data, index) => {
    return (
      <CourseCard
        key={index}
        id={data.id}
        type={data.type}
        title={data.name}
        price={data.price}
        url={data.image}
      />
    );
  });
}
function AnotherCourseList({ title, courseData = [] }) {
  console.log(courseData);
  return (
    <Box sx={styles.container}>
      <Typography
        color="#790B0A"
        sx={{
          fontSize: { sm: "32px", xs: "24px" },
          fontWeight: "600",
        }}
      >
        {title}
      </Typography>
      <Box sx={styles.cardList}>{createList(courseData)}</Box>
    </Box>
  );
}

export default AnotherCourseList;
