import { Box, InputLabel, MenuItem, Select } from "@mui/material";
import React, { useState, useEffect } from "react";

function DateDropdown() {
  const [dates, setDates] = useState([]);

  useEffect(() => {
    const today = new Date();
    today.setDate(today.getDate() + 1);

    const dateOptions = Array.from({ length: 7 }, (_, i) => {
      const date = new Date();
      date.setDate(today.getDate() + i);
      const day = date.toLocaleString("en-US", { weekday: "long" });
      const month = date.toLocaleString("en-US", { month: "long" });
      const dayOfMonth = date.getDate();
      const year = date.getFullYear();
      return `${day}, ${dayOfMonth} ${month} ${year}`;
    });
    setDates(dateOptions);
  }, []);

  return (
    <Box>
      <InputLabel id="schedule">Select Schedule</InputLabel>
      <Select
        id="schedule"
        label="Select Schedule"
        value={undefined}
        sx={{ width: "300px", minWidth: "200px" }}
      >
        {dates.map((date, index) => (
          <MenuItem key={index} value={date}>
            {date}
          </MenuItem>
        ))}
      </Select>
    </Box>
  );
}

export default DateDropdown;
