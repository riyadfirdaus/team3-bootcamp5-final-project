import {
  Box,
  Card,
  CardActionArea,
  CardMedia,
  Typography,
} from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

export default function CarTypeCard(props) {
  const path = `/coursemenu/${props.id}`;

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        textDecoration: "none",
        color: "inherit",
      }}
    >
      <Card sx={{ width: 100 }}>
        <CardActionArea
          component={Link}
          to={{
            pathname: path,
          }}
          state={{
            id: props.id,
          }}
        >
          <CardMedia component="img" image={props.url} alt={props.type} />
        </CardActionArea>
      </Card>
      <Typography
        sx={{
          fontSize: 16,
          fontWeight: "600",
          textAlign: "center",
          padding: 2,
        }}
      >
        {props.type}
      </Typography>
    </Box>
  );
}
