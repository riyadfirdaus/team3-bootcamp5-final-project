import { Container, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCarTypes, selectAllTypes } from "../../Redux/CarTypeSlice";
import CarTypeCard from "./CarTypeCard";

let styles = {
  container: {
    display: "flex",
    flexFlow: "column",
    gap: "80px",
    justifyContent: "center",
    alignItems: "center",
    margin: "64px 91px",
  },
  cardList: {
    display: "flex",
    flexFlow: "row wrap",
    gap: "100px",
    padding: "24px",
    justifyContent: "center",
  },
};

function CarTypeList(props) {
  const { title } = props;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchCarTypes());
  }, [dispatch]);

  const carTypeData = useSelector(selectAllTypes);

  return (
    <Container maxWidth="xl">
      <Typography
        color="primary"
        sx={{
          fontSize: 32,
          fontWeight: "600",
          textAlign: "center",
          marginBottom: "80px",
        }}
      >
        {title}
      </Typography>
      <Container maxWidth="md" style={styles.cardList}>
        {carTypeData.map((data) => {
          return (
            <CarTypeCard
              key={data.id}
              id={data.id}
              type={data.name}
              url={data.imageIcon}
              desc={data.description}
            />
          );
        })}
      </Container>
    </Container>
  );
}

export default CarTypeList;
