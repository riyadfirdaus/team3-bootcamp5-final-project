import { Container, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

const styles = {
  container: {
    display: "grid",
    gridTemplateColumns: { md: "2fr 1fr", xs: "1fr" },
    gridTemplateRows: { md: "1fr", xs: "1fr auto" },
    gap: "24px",
    alignItems: "center",
    margin: { md: "128px 0px 128px 102px", xs: "64px 32px 64px 32px" },
  },
  textBox: {
    display: "flex",
    flexFlow: "column",
    gap: "24px",
  },
  imageContainer: {
    position: "relative",
    display: "flex",
    flexFlow: "column",
    justifyContent: "flex-end",
    alignItems: { md: "flex-start", xs: "center" },
  },
  image: {
    maxWidth: "280px",
  },
  rectangleSmall: {
    position: "absolute",
    height: { sm: "40px", xs: "32px" },
    width: "100vw",
    left: { md: "0", xs: "-32px" },
    bottom: { md: "140px", xs: "100px" },
    backgroundColor: "#790B0A",
    zIndex: "-1",
  },
  rectangleBig: {
    position: "absolute",
    height: { md: "100px", xs: "64px" },
    width: "100vw",
    left: { md: "0", xs: "-32px" },
    bottom: "0",
    backgroundColor: "#790B0A",
    zIndex: "-1",
  },
};

function Benefit() {
  return (
    <>
      <Box sx={styles.container}>
        <Box sx={styles.textBox}>
          <Typography color="#790B0A" sx={{ fontSize: 40, fontWeight: "600" }}>
            Gets your best benefit
          </Typography>
          <Typography>
            Looking to improve your driving skills and gain expert knowledge
            about cars? Look no further than our car course website! Our courses
            are taught by experienced professionals who will teach you
            everything you need to know about driving and maintaining your
            vehicle. Whether you&apos;re a beginner or an experienced driver,
            our comprehensive courses will help you become a better, safer
            driver and give you the confidence to hit the road with ease. Sign
            up today and take the first step towards becoming a skilled and
            knowledgeable driver!
          </Typography>
        </Box>
        <Container sx={styles.imageContainer}>
          <img style={styles.image} src="/images/image 9.png" />
          <Box sx={styles.rectangleSmall}></Box>
          <Box sx={styles.rectangleBig}></Box>
        </Container>
      </Box>
    </>
  );
}

export default Benefit;
