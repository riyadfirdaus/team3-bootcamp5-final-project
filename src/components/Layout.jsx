import React from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { ToastContainer } from "react-toastify";

// path yang tidak memakai navbar
const navbar = [{ path: "404" }];
// path yang tidak memakai footer
const footer = [{ path: "Checkout" }, { path: "404" }];

export default function Layout({ title, children }) {
  const pageTitle = `${title} | Otomobil`;

  function checkPath({ path }) {
    return path === title;
  }

  return (
    <HelmetProvider>
      {/* <Helmet>
        <title>{pageTitle}</title>
      </Helmet> */}
      {navbar.find(checkPath) === undefined ? <Navbar /> : ""}
      <ToastContainer position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light" />
      {children}
      {footer.find(checkPath) === undefined ? <Footer /> : ""}
    </HelmetProvider>
  );
}
