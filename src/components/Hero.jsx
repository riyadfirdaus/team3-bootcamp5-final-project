import { Typography } from "@mui/material";
import Box from '@mui/material/Box';
import {
  createTheme,
  responsiveFontSizes
} from '@mui/material/styles';
import React, { useEffect, useState } from "react";
import BannerBg from '../assets/bgBanner.png';

const Hero = () => {
  const [navbarHeight, setNavbarHeight] = useState(0);
  useEffect(() => {
    const navbar = document.querySelector('#navbar');
    if (navbar) {
      const height = navbar.getBoundingClientRect().height;
      setNavbarHeight(height);
    }
  }, []);

    let theme = createTheme();
    theme = responsiveFontSizes(theme);
    const dataText = [
      {
        upper : '50+',
        lower : "A class ready to make you a reliable driver"
      },
      {
        upper: '20+',
        lower: 'Professional workforce with great experience'
      },
      {
        upper: '10+',
        lower: 'Cooperate with driver service partners'
      }
    ]

  return (
    <div
    style={{
      backgroundImage: `url(${BannerBg})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      height: 'calc(100vh - 64px)', // Subtract the height of the AppBar
      color: 'white'
    }}
  >
    <Typography
      variant="h2"
      sx={{
        textAlign: 'center',
        paddingTop: { xs: '20vh', sm: '25vh' },
        fontSize: { xs: '1.5rem', md: '2rem' },
      }}
    >
      We provide driving lessons for various types of cars
    </Typography>
    <Typography
      variant="h5"
      sx={{
        textAlign: 'center',
        paddingBottom: { xs: '5vh', sm: '8vh' },
        fontSize: { xs: '1.5rem', sm: '1.75rem' },
        fontStyle: 'normal',
        marginTop: '1.5rem'
      }}
    >
      Professional staff who are ready to help you to become a much-needed reliable driver
    </Typography>
    <Box sx={{
      display: 'flex'
    }}>
      {dataText.map((dat) => {
        return(
          <div key={dat.upper}>
            <Typography
      variant="h2"
      sx={{
        textAlign: 'center',
        paddingTop: { xs: '3vh', sm: '8vh' },
        fontSize: { xs: '1.5rem', md: '2rem' },
      }}
    >
      {dat.upper}
    </Typography>
    <Typography
      variant="h5"
      sx={{
        textAlign: 'center',
        paddingBottom: { xs: '1vh', sm: '5vh' },
        fontSize: { xs: '1rem', sm: '1.5rem' },
        fontStyle: 'normal',
      }}
    >
      {dat.lower}
    </Typography>
          </div>
        )
      })}
    </Box>
  </div>
  );
};

export default Hero;
