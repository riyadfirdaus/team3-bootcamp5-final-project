import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import useFormattedDate from "../customHook/useFormatedDate";

export const TableDate = (props) => {
  const { date } = props;

  return <TableCell align="center">{useFormattedDate(date)}</TableCell>;
};
