import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import React from "react";

function ClassCard({data}) {
  const value = data
  return (
    <Card>
      <CardActionArea>
        <Box
          sx={{
            display: "flex",
            flexFlow: "row wrap",
            justifyContent: "flex-start",
            margin: "40px 71px 24px",
          }}
        >
          <CardMedia
            component="img"
            sx={{ width: 200 }}
            image={value?.image}
          />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "4px",
              }}
            >
              <Typography sx={{ fontSize: 14 }} color="text.secondary">
                {value?.type}
              </Typography>
              <Typography
                sx={{ fontSize: 16, fontWeight: "600" }}
                gutterBottom
                component="div"
              >
                {value?.name}
              </Typography>
            </Box>
            <Typography
              sx={{ fontSize: 20, fontWeight: "500" }}
              color="#790B0A"
              gutterBottom
              component="div"
            >
              {dayjs(value?.schedule).format('dddd, DD MMMM YYYY')}
            </Typography>
          </CardContent>
        </Box>
      </CardActionArea>
    </Card>
  );
}

export default ClassCard;
