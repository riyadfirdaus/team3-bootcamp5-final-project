import { ThemeProvider } from "@emotion/react";
import {
  Box,
  Checkbox,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  createTheme,
} from "@mui/material";
import { visuallyHidden } from "@mui/utils";
import PropTypes from "prop-types";
import React from "react";

const headCells = [
  {
    id: "paymentId",
    numeric: true,
    disablePadding: true,
    label: "Id",
    align: "left",
    sort: true,
  },
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Name",
    align: "right",
    sort: true,
  },
  {
    id: "logo",
    numeric: false,
    disablePadding: false,
    label: "Image Url",
    align: "center",
    sort: false,
  },
  {
    id: "status",
    numeric: true,
    disablePadding: false,
    label: "Status",
    align: "center",
    sort: true,
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    align: "center",
    sort: false,
  },
];

function EnhancedTableHead(props) {
  const {
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;

  const createSortHandler = (newOrderBy) => (event) => {
    onRequestSort(event, newOrderBy);
  };

  const theme = createTheme({
    overrides: {
      MuiTableCell: {
        styleOverrides: {
          head: {
            backgroundColor: "#FFFFFF",
            color: "#790B0A",
            fontWeight: 800,
          },
        },
      },
      MuiTableRow: {
        styleOverrides: {
          root: {
            "& .MuiTableCell-body": {
              color: "#4F4F4F",
            },
            "&:nth-of-type(even)": {
              backgroundColor: "#790B0A1A ",
            },
            // hide last border
            "&:last-child td, &:last-child th": {
              border: 1,
            },
          },
        },
      },
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              color="primary"
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{
                "aria-label": "select all desserts",
              }}
            />
          </TableCell>

          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.align}
              padding={headCell.disablePadding ? "none" : "normal"}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              {headCell.sort == true ? (
                <TableSortLabel
                  active={orderBy === headCell.id}
                  direction={orderBy === headCell.id ? order : "asc"}
                  onClick={createSortHandler(headCell.id)}
                  style={{ minWidth: "50px" }}
                >
                  {headCell.label}
                  {orderBy === headCell.id ? (
                    <Box component="span" sx={visuallyHidden}>
                      {order === "desc"
                        ? "sorted descending"
                        : "sorted ascending"}
                    </Box>
                  ) : null}
                </TableSortLabel>
              ) : (
                headCell.label
              )}
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    </ThemeProvider>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

export default EnhancedTableHead;
