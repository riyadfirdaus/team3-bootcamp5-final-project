import EditIcon from "@mui/icons-material/Edit";
import {
  Button,
  Checkbox,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  Switch,
  TableCell,
  TableRow,
  TextField,
} from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchpayments } from "../../../Redux/PaymentSlice";
import DeleteButton from "../../Button/DeleteButton";

function EnhancedTableRow(props) {
  const {
    handleClick,
    handleRemoveSelected,
    row,
    itemSelected,
    isItemSelected,
    labelId,
  } = props;
  const [open, setOpen] = React.useState(false);
  const [isChecked, setChecked] = useState(row.status);
  const [isDisabled, setDisabled] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [formData, setFormData] = useState({
    id: row.id,
    name: row.name,
    logo: row.logo,
    status: row.status,
  });

  // useEffect(() => {
  //   console.log(formData);
  // }, [formData]);

  const [image, setImage] = useState("");
  const [url, setUrl] = useState(row.logo);
  const uploadImage = () => {
    return new Promise(function (resolve, reject) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "otomobil");
      data.append("cloud_name", "dws0flefe");
      fetch("https://api.cloudinary.com/v1_1/dws0flefe/image/upload", {
        method: "post",
        body: data,
      })
        .then((resp) => resp.json())
        .then((data) => {
          resolve(data.url);
        })
        .catch((err) => reject(err));
    });
  };

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    setFormData({ ...formData, logo: file });
  };

  const handleToggleChange = (event) => {
    const { checked } = event.target;
    setChecked(checked);
    setFormData({ ...formData, status: checked });
  };

  const handleCancel = () => {
    setFormData({
      id: row.id,
      name: row.name,
      logo: row.logo,
      status: row.status,
    });
    setChecked(row.status);
    setImage("");
    setDisabled(false);
    setOpen(false);
  };

  const dispatch = useDispatch();
  const { currentUser } = useSelector((state) => state.auth);
  const handleSubmit = async (event) => {
    event.preventDefault();
    const { id, name, logo, status } = formData;
    try {
      if (image == "") {
        setUrl(logo);
      } else {
        setUrl(await uploadImage());
      }
      const response = await axios.put(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/PaymentMethods`,
        {
          name: `${name}`,
          logo: `${url}`,
          status: status,
        },
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
          },
          params: {
            id,
          },
        }
      );
      console.log(response.data);
    } catch (error) {
      console.log(error.response);
    }
    dispatch(fetchpayments());
    setImage("");
    setOpen(false);
    setDisabled(false);
  };

  return (
    <TableRow
      hover
      onClick={(event) => handleClick(event, row)}
      aria-checked={isItemSelected}
      tabIndex={-1}
      key={row.name}
      selected={isItemSelected}
      sx={{ cursor: "pointer" }}
    >
      {/* Checkbox */}
      <TableCell padding="checkbox">
        <Checkbox
          color="primary"
          checked={isItemSelected}
          inputProps={{
            "aria-labelledby": labelId,
          }}
        />
      </TableCell>
      {/* Id */}
      <TableCell component="th" id={labelId} scope="row" padding="none">
        {row.id}
      </TableCell>
      {/* Nama */}
      <TableCell align="right">{row.name}</TableCell>
      {/* Image Link */}
      <TableCell
        align="center"
        onClick={(event) => {
          event.stopPropagation();
        }}
      >
        <Chip
          label="Link"
          component="a"
          href={url}
          target="_blank"
          variant="outlined"
          clickable
        />
      </TableCell>
      {/* Status */}
      <TableCell align="center">
        {row.status == "1" ? (
          <Chip label="active" color="success" />
        ) : (
          <Chip label="inactive" color="warning" />
        )}
      </TableCell>
      {/* Action */}
      <TableCell
        align="center"
        onClick={(event) => {
          event.stopPropagation();
        }}
      >
        <IconButton aria-label="edit" onClick={handleOpen}>
          <EditIcon />
        </IconButton>

        <DeleteButton selectedItem={[row]} handleClick={handleRemoveSelected} />

        {/* DIALOG */}
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Edit Payment Method</DialogTitle>
          <DialogContent>
            <TextField
              defaultValue={row.name}
              margin="dense"
              onChange={(e) =>
                setFormData({ ...formData, name: e.target.value })
              }
              id="name"
              label="Payment Method Name"
              type="name"
              fullWidth
              variant="standard"
            />
            <TextField
              sx={{ marginBottom: "16px" }}
              defaultValue={row.logo}
              onChange={(e) =>
                setFormData({ ...formData, logo: e.target.value })
              }
              margin="dense"
              id="name"
              label="Logo Url"
              type="name"
              fullWidth
              variant="standard"
              disabled={isDisabled}
            />
            <TextField
              sx={{ marginBottom: "16px" }}
              onChange={(e) => {
                setImage(e.target.files[0]);
                setDisabled(true);
              }}
              margin="dense"
              id="name"
              label="Logo Url"
              type="file"
              fullWidth
              variant="standard"
            />
            <FormControlLabel
              control={
                <Switch checked={isChecked} onChange={handleToggleChange} />
              }
              label="Active"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCancel}>Cancel</Button>
            <Button onClick={handleSubmit}>Save</Button>
          </DialogActions>
        </Dialog>
      </TableCell>
    </TableRow>
  );
}

export default EnhancedTableRow;
