import AddIcon from "@mui/icons-material/Add";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  Switch,
  TextField,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import axios from "axios";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchpayments } from "../../../Redux/PaymentSlice";
import DeleteButton from "../../Button/DeleteButton";

function EnhancedTableToolbar({ itemSelected, numSelected, handleClick }) {
  const [image, setImage] = useState("");
  const [formData, setFormData] = useState({
    name: "",
    logo: "",
    status: false,
  });

  useEffect(() => {
    console.log(formData);
  }, [formData]);

  const uploadImage = () => {
    return new Promise(function (resolve, reject) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "otomobil");
      data.append("cloud_name", "dws0flefe");
      fetch("https://api.cloudinary.com/v1_1/dws0flefe/image/upload", {
        method: "post",
        body: data,
      })
        .then((resp) => resp.json())
        .then((data) => {
          resolve(data.url);
        })
        .catch((err) => reject(err));
    });
  };

  //Handle Modal Dialog
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setFormData({
      name: "",
      logo: "",
      status: false,
    });
    setOpen(false);
  };

  //Handle Input
  const handleToggleChange = (event) => {
    const { checked } = event.target;
    setFormData({ ...formData, status: checked });
  };

  // Handle Submit
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state) => state.auth);

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const imageUrl = await uploadImage();
      const response = await axios.post(
        `${process.env.REACT_APP_BASE_API_URL}/api/Admin/PaymentMethods`,
        {
          name: `${formData.name}`,
          logo: `${imageUrl}`,
          status: formData.status,
        },
        {
          headers: {
            Authorization: `Bearer ${currentUser.token}`,
          },
          params: {},
        }
      );
      console.log(response.data);
    } catch (error) {
      console.log(error.response);
    }
    dispatch(fetchpayments());
    handleClose();
  };
  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        backgroundColor: "#790B0A",
        color: "#FFFFFF",
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: "1 1 100%" }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{ flex: "1 1 100%" }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Payment Method
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <DeleteButton
            selectedItem={itemSelected}
            buttonColor={"white"}
            handleClick={handleClick}
          />
        </Tooltip>
      ) : (
        <Tooltip title="Add Item">
          <IconButton onClick={handleOpen}>
            <AddIcon style={{ color: "white" }} />
          </IconButton>
        </Tooltip>
      )}

      {/* DIALOG */}
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Payment Method</DialogTitle>
        <DialogContent>
          <TextField
            onChange={(e) => setFormData({ ...formData, name: e.target.value })}
            margin="dense"
            id="name"
            label="Payment Method Name"
            type="name"
            fullWidth
            variant="standard"
          />
          <TextField
            sx={{ marginBottom: "16px" }}
            defaultValue={formData.logo}
            onChange={(e) => setImage(e.target.files[0])}
            margin="dense"
            id="name"
            label="Logo Url"
            type="file"
            fullWidth
            variant="standard"
          />
          <FormControlLabel
            control={<Switch onChange={handleToggleChange} />}
            label="Active"
          />
          <Box sx={{ display: "flex", justifyContent: "space-between" }}></Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Save</Button>
        </DialogActions>
      </Dialog>
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

export default EnhancedTableToolbar;
