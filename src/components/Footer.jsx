import { ThemeProvider } from "@emotion/react";
import {
  Instagram,
  MailOutline,
  Phone,
  SendOutlined,
  YouTube,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  Container,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemText,
  Stack,
  Typography,
  createTheme,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { selectAllTypes } from "../Redux/CarTypeSlice";

const theme = createTheme({
  palette: {
    primary: {
      main: "#790B0A",
    },
    secondary: {
      main: "#FFFFFF",
    },
  },
  typography: {
    fontFamily: ["Poppins"],
  },
});

const sosmed = [
  {
    Url: "#",
    Icon: <Phone />,
  },
  {
    Url: "#",
    Icon: <Instagram />,
  },
  {
    Url: "#",
    Icon: <YouTube />,
  },
  {
    Url: "#",
    Icon: <SendOutlined />,
  },
  {
    Url: "#",
    Icon: <MailOutline />,
  },
];

function Footer() {
  const data = useSelector(selectAllTypes);
  const firstHalf = data.slice(0, Math.ceil(data.length / 2));
  const secondHalf = data.slice(Math.ceil(data.length / 2));

  return (
    <ThemeProvider theme={theme}>
      <Divider />
      <footer>
        <Container maxWidth="xl" sx={{ paddingY: "24px" }}>
          <Grid
            container
            spacing={{ xs: 4, md: 12 }}
            columns={{ xs: 1, sm: 8, md: 12 }}
          >
            <Grid item xs={2} sm={4} md={4}>
              <Typography color="primary" variant="h6" gutterBottom>
                About Us
              </Typography>
              <Typography variant="body2" sx={{ textAlign: "justify" }}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo.
              </Typography>
            </Grid>
            <Grid item xs={2} sm={4} md={4}>
              <Typography color="primary" variant="h6" gutterBottom>
                Product
              </Typography>
              <Box display={"flex"}>
                {/* List satu */}
                <List dense={true}>
                  {firstHalf.map((data, index) => (
                    <ListItem
                      component={Link}
                      to={{
                        pathname: `/coursemenu/${data.id}`,
                      }}
                      state={{
                        id: data.id,
                      }}
                      sx={{ color: "inherit" }}
                      key={index}
                    >
                      <ListItemText primary={data.name} />
                    </ListItem>
                  ))}
                </List>
                {/* List dua */}
                <List dense={true}>
                  {secondHalf.map((data, index) => (
                    <ListItem
                      component={Link}
                      to={{
                        pathname: `/coursemenu/${data.id}`,
                      }}
                      state={{
                        id: data.id,
                      }}
                      sx={{ color: "inherit" }}
                      key={index}
                    >
                      <ListItemText primary={data.name} />
                    </ListItem>
                  ))}
                </List>
              </Box>
            </Grid>
            <Grid item xs={2} sm={4} md={4}>
              <Typography color="primary" variant="h6" gutterBottom>
                Address
              </Typography>
              <Typography variant="body2">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque.
              </Typography>
              <Typography
                sx={{ paddingTop: "16px" }}
                color="primary"
                variant="h6"
                gutterBottom
              >
                Contact Us
              </Typography>
              <Stack direction="row" spacing={2}>
                {sosmed.map((data, index) => (
                  <Link to={data.Url} key={index}>
                    <Avatar
                      style={{
                        backgroundColor: theme.palette.primary.main,
                      }}
                    >
                      {data.Icon}
                    </Avatar>
                  </Link>
                ))}
              </Stack>
            </Grid>
          </Grid>
        </Container>
      </footer>
    </ThemeProvider>
  );
}

export default Footer;
