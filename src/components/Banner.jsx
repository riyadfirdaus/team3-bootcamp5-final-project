import React from "react";
import Box from "@mui/material/Box";
import bgBanner from "../assets/bgBanner.png";
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from "@mui/material/styles";
import { Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";

let theme = createTheme();
theme = responsiveFontSizes(theme);

const Banner = () => {
  return (
    <Box
      style={{
        backgroundImage: `url(${bgBanner})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          color: "#FFFFFF",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          gap: { sm: "35px" },
        }}
      >
        <ThemeProvider theme={theme}>
          <Typography variant="h4" textAlign="center">
            We provide driving lessons for various types of cars
          </Typography>
          <Typography variant="h5" textAlign="center">
            Professional staff who are ready to help you to become a much-needed
            reliable driver
          </Typography>
        </ThemeProvider>
      </Box>
      <Box
        sx={{ display: "flex", justifyContent: "space-between", gap: "25px" }}
      >
        <Card
          sx={{ minWidth: 275, backgroundColor: "transparent", color: "white" }}
        >
          <CardContent
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              gap: "20px",
              color: "white",
            }}
          >
            <Typography variant="h2" component="div">
              50+
            </Typography>
            <Typography textAlign="center" variant="h5" sx={{ mb: 1.5 }}>
              A class ready to make you a reliable driver
            </Typography>
          </CardContent>
        </Card>
        <Card
          sx={{ minWidth: 275, backgroundColor: "transparent", color: "white" }}
        >
          <CardContent
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              gap: "20px",
              color: "white",
            }}
          >
            <Typography variant="h2" component="div">
              20+
            </Typography>
            <Typography textAlign="center" variant="h5" sx={{ mb: 1.5 }}>
              Professional workforce with great experience
            </Typography>
          </CardContent>
        </Card>
        <Card
          sx={{ minWidth: 275, backgroundColor: "transparent", color: "white" }}
        >
          <CardContent
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              gap: "20px",
              color: "white",
            }}
          >
            <Typography variant="h2" component="div">
              10+
            </Typography>
            <Typography textAlign="center" variant="h5" sx={{ mb: 1.5 }}>
              Cooperate with driver service partners
            </Typography>
          </CardContent>
        </Card>
      </Box>
    </Box>
  );
};

export default Banner;
