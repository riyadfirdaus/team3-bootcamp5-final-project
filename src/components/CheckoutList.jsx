import { Box, Card, CardMedia, Typography } from "@mui/material";
import React from "react";
import useFormattedDate from "../customHook/useFormatedDate";

export default function CheckoutList(props) {
  const { img, type, brand, schedule, price } = props;

  return (
    <Box
      sx={{
        display: "flex",
        textDecoration: "none",
        alignItems: "center",
        gap: 2,
      }}
    >
      <Card sx={{ width: 200 }}>
        <CardMedia component="img" image={img.url} alt={img.alt} />
      </Card>
      <Box>
        <Typography variant="subtitle2" color="secondary">
          {type}
        </Typography>
        <Typography variant="h6" sx={{ fontWeight: 600 }}>
          {brand}
        </Typography>
        <Typography variant="subtitle1" color="secondary">
          {useFormattedDate(schedule)}
        </Typography>
        <Typography color="primary" sx={{ fontWeight: 600 }}>
          IDR {price.toLocaleString("id-ID")}
        </Typography>
      </Box>
    </Box>
  );
}
