import { Box, Typography } from "@mui/material";
import React from "react";

function DescriptionBox(props) {
  return (
    <Box>
      <Typography sx={{ fontSize: 24, fontWeight: "600" }} gutterBottom>
        {props.title}
      </Typography>
      <Typography variant="body1" gutterBottom>
        {props.desc}
      </Typography>
    </Box>
  );
}

export default DescriptionBox;
