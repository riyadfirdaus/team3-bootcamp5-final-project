# Otomobil

🌱 Tugas ini dibuat untuk memenuhi Final Project Fullstack Bootcamp #5 [Coding.id](http://coding.id/)

🔗 **Link** 
- Figma: [Click Here](https://www.figma.com/file/D6iRcI7ocfftICoI3xtXDC/Otomobil)
- Backend: [Click Here](https://gitlab.com/riyadfirdaus/team3-netcore-api)


## Deskripsi
Aplikasi Otomobil merupakan aplikasi yang menyediakan kursus menyetir mobil. Aplikasi ini dirancang untuk membantu pengemudi baru ataupun yang berpengalaman untuk dapat menguasai keterampilan menyetir berbagai jenis mobil seperti mobil sedan, SUV, truk, dan lain-lain.


## Stack 
- Front-end: React.js and Material UI
- Back-end: .NET
- Database: Microsoft SQL Server

## Kontributor
Projek ini dibuat oleh:

<table>
    <thead>
        <tr>
            <th></th>
            <th>Nama</th>
            <th>Role</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Fitra Nurakbar</td>
            <td>Full-Stack</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Ranu Agita Fahmi</td>
            <td>Full-Stack</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Riyad Firdaus</td>
            <td>Full-Stack</td>
        </tr>
    </tbody>
</table>


